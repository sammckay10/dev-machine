" -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
" You are viewing the ~/.vimrc of Sam McKay. _-_-_-_-_-_-_-_-_-_-_-_-_-_
" -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
" https://gitlab.com/sammckay10/vim-configuration/-/blob/master/.vimrc _
" -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_

" Install ag for FZF and Ack usage
if !executable('ag')
  if has('macunix')
    echo 'Running brew install the_silver_searcher'
    call system('brew install the_silver_searcher')
  else
    echo 'Running sudo apt-get install silversearcher-ag'
    call system('sudo apt-get install silversearcher-ag')
  endif
endif

" Install Plug if it's not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Load plugins
call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ntpeters/vim-better-whitespace'
Plug 'christoomey/vim-system-copy'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'nlknguyen/papercolor-theme'
Plug 'scrooloose/nerdcommenter'
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/lightline.vim'
Plug 'sheerun/vim-polyglot'
Plug 'scrooloose/nerdtree'
Plug 'roman/golden-ratio'
Plug 'tpope/vim-fugitive'
Plug 'mileszs/ack.vim'
Plug 'junegunn/fzf'

" Stop loading plugins
call plug#end()

" Auto reload vim if .vimrc is changed
au BufWritePost .vimrc so $MYVIMRC

" Set $MYVIMRC to open the actual .vimrc file
let $MYVIMRC = '~/.vimrc'

" CoC extensions
let g:coc_global_extensions = [
      \'coc-spell-checker',
      \'coc-tailwindcss',
      \'coc-highlight',
      \'coc-prettier',
      \'coc-tsserver',
      \'coc-vimlsp',
      \'coc-svelte',
      \'coc-phpls',
      \'coc-pairs',
      \'coc-emmet',
      \'coc-json',
      \'coc-css',
      \'coc-html',
      \'coc-svg',
      \]

" CoC config
let g:coc_user_config = {
      \'coc.preferences.formatOnSaveFiletypes': ['*'],
      \}

" Change leader key
let mapleader = ' '

" Remove key mapping for recording function
map q <Nop>

" Map Ctrl+p to FZF
nnoremap <silent> <C-p> :FZF -m<cr>

" Toggle NERDTree with Ctrl+b
nnoremap <silent> <C-b> :NERDTreeToggle<cr>

" Insert CoC suggestions with tab or enter
inoremap <silent><expr> <Tab> coc#pum#visible() ? coc#pum#confirm() : '<Tab>'
inoremap <silent><expr> <Enter> coc#pum#visible() ? coc#pum#confirm() : '<Enter>'

" NERDTree settings
let g:NERDTreeWinPos = 'left'
let g:NERDTreeQuitOnOpen = 1

" NERDCommenter settings
let g:NERDTrimTrailingWhitespace = 1
let g:NERDCommentEmptyLines = 1
let g:NERDDefaultAlign = 'left'
let g:NERDSpaceDelims = 1

" Automatically trim white space & blank lines
let g:better_whitespace_enabled = 1
let g:strip_whitespace_confirm = 0
let g:strip_whitespace_on_save = 1
let g:strip_whitelines_at_eof = 1

" Set Lightline theme
let g:lightline = {
      \ 'colorscheme': 'PaperColor',
      \ }

" Tell Ack to use ag
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Tell FZF to use ag and ignore files in .gitignore
let $FZF_DEFAULT_COMMAND = 'ag -g ""'

" Theme bits
colorscheme PaperColor

" General Vim settings
set completeopt=preview
set relativenumber
set softtabstop=2
set nowritebackup
set noerrorbells
set novisualbell
set shiftwidth=2
set cmdheight=1
set scrolloff=8
set lazyredraw
set noshowmode
set nohlsearch
set ignorecase
set expandtab
set infercase
set tabstop=2
set number
set nowrap
